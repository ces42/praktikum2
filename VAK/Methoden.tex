\section{Einleitung}
Als \emph{Vakuum} bezeichnet man einen fast leeren Raum d.\, h.\ ein Raum in dem sich erheblich weniger Gas befindet als in der üblichen Umgebung.

\section{Methoden}

\subsection{Gase}
Die \emph{Zustandsgleichung idealer Gase}
%
\begin{equation}
	p V = N k T
\end{equation}
%
gibt die Beziehung zwischen Druck $p$, Volumen $V$ und Temperatur $T$ für ein Gas von $N$ Teilchen. $k$ bezeichnet die Boltzmannkonstante. Die Gleichung gibt für eine Feste Temperatur eine Äquivalenz zwischen Drücken und Teilchendichten $N/V$. Daher kann (bei fester Temperatur) der Druck statt der Teilchenzahl als charakteristische Größe eines Vakuums angegeben werden. Dadurch teilt man Üblicherweise den Druckbereich $\SIrange{0}{1013}{hPa}$ in Vakua verschiedener Qualität ein:
%
\begin{itemize}
 	\item \emph{Grobvakuum:} $\SIrange{1}{300}{hPa}$ \quad (Teilchendichte ca. $\SIrange{e16}{e19}{cm^{-3}}$, $\lambda < \SI{100}{\micro m}$)
    \item \emph{Feinvakuum:} $\SIrange{e-3}{1}{hPa}$ \quad (Teilchendichte ca. $\SIrange{e13}{e16}{cm^{-3}}$, $\lambda = \SI{100}{\micro m} - \SI{10}{cm}$)
    \item \emph{Hochvakuum:} $\SIrange{e-7}{e-3}{hPa}$ \quad (Teilchendichte ca. $\SIrange{e19}{e13}{cm^{-3}}$, $\lambda  = \SI{10}{cm} - \SI{1}{km}$)
    \item \emph{Ultrahochvakuum:} $<\SI{e-7}{hPa}$ \quad (Teilchendichte $< \SI{e-9}{cm^{-3}}$, $\lambda > \SI{1}{km}$)
\end{itemize}
%
Als \emph{mittlere freie Weglänge} $\lambda$ bezeichnet man die Strecke die ein Gasteilchen im Mittel zurücklegt bis es mit einem anderen zusammenstößt. Aus der kinetischen Gastheorie folgt mit Teilchenradius $r$ (wir nehmen vereinfachend Kugelförmige Moleküle an) und Teilchendichte $\rho$
%
\begin{equation}
	\lambda = \frac1{4 \sqrt{2} \pi r^2 \rho} ~. \label{eq:lambda}
\end{equation}
%

Die mittlere kann als eine zentrale Größe der kinetischen Gastheorie verwendet werden um Viskosität und Wärmeleitfähigkeit eines Gases zu berechnen. Für die Wärmeleitungskonstante $\kappa$ und Viskosität $\eta$ eines Gases gelten üblicherweise (siehe \cite{a})
%
\begin{align}
	\kappa &= \frac12 \lambda \rho k \bar{v} \label{eq:wleit}\\
    \eta &= \frac13 \lambda \rho m \bar{v} \label{eq:visk}
\end{align}
%
$m$ bezeichnet dabei die Masse der Gasteilchen und $\bar{v} = \sqrt{\frac{8 k T}{\pi m}}$ deren mittlere Geschwindigkeit. Setzt man \cref{eq:lambda} erhält man, dass $\kappa$ und $\eta$ unabhängig von der Dichte des Gases sind.
    
\Cref{eq:lambda} gilt jedoch nur wenn die rechte Seite viel kleiner als die relevante Größenskala $S$ (Dicke der wärmeleitenden Schicht bzw. Rohrdurchmesser für die Viskosität) ist. Ist dies nicht der Fall, so ist $\lambda \approx S$. In diesem Fall sind $\kappa$ und $\eta$ direkt proportional zur Dichte.

\subsection{Gasströmung durch Rohre}
Für die Strömung von Gas durch ein Rohr mit Durchmesser $d$ und Länge $\l$ folgt aus dem Hagen-Poiseuilleschen Gesetz (siehe \cite{a}) für den Gasmengenstrom $Q = p \dot{V}$
%
\begin{equation}
	Q = \frac{\pi d^4}{128 \eta \l} \bar{p} \Delta p =: L \Delta p \label{eq:leit}
\end{equation}
%
Wobei $\Delta p$ die Druckdifferenz zwischen Anfang und ende des Rohres ist und $\bar{p}$ der mittlere Druck entlang des Rohres. $L$ bezeichnet man als Leitwert des Rohres. \Cref{eq:leit} gilt jedoch nur wenn $\lambda \ll d$. Der andere Fall wird als Molekulare Strömung bezeichnet (vgl. \cref{sec:frage1}); Hier gilt
%
\begin{equation}
	L = \sqrt{\frac{\pi k T}{18 m}} \frac{d^3}{\l} \label{eq:leit_molek}
\end{equation}
% 
Analog zum elektrischen Strom addieren sich die Leitwerte von Rohren in einer "`Parallelschaltung"' und die Inverse der Leitwerte in einer "`Reihenschaltung"'.

\subsection{Pumpe}
Die \emph{Saugleistung} $Q_\text{p}$ und das \emph{Saugvermögen} $S$ einer Pumpe sind definiert als
%
\begin{equation}
	Q_\text{p}= S p = - \frac{\d (p V)}{\d t} \label{eq:pumpe}
\end{equation}
%
In der Praxis wird das \emph{effektive} Saugvermögen noch durch den endlichen Gesamtleitwert $L$ der zur Pumpe führenden Rohre reduziert. Es gilt
%
\begin{equation}
	\frac1{S_\text{eff}} = \frac1{S} + \frac1{L} \label{eq:leitadd}
\end{equation}
%

Will man ein Volumen $V$ mit einer Pumpe auspumpen so ergibt \cref{eq:pumpe}
%
\begin{equation}
	V \frac{\d p}{\d t} = -S \cdot p \quad \Longrightarrow\quad  p(t) = p(0) \exp \left( -\frac{S_\text{eff}}{V} t \right) \label{eq:auspump}
\end{equation}
%

\subsection{Vakuummanometer}
\label{leist}
\paragraph{Pirani-/Wärmeleitungsmanometer}
Bei diesem Manometer befindet sich ein Draht in einem zylindrischen Gefäß, das mit dem Gas (dessen Druck gemessen werden soll) gefüllt ist. Eine Schaltung regelt den Strom durch den Draht so, dass der Widerstand und dementsprechend auch die Temperatur des Drahtes immer gleich ist. Die Leistung $\frac14 R I^2$ ist daher proportional zur Wärmeleitfähigkeit des Gases

