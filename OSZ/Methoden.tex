\section{Einleitung}
Ein Oszilloskop ist ein Messgerät, mit dem elektrische Spannungen angezeigt werden können, mit denen man beispielsweise Durchlasskurven von Wechselstromschaltungen messen kann.
\section{Theoretische Grundlagen}
\subsection{Aufbau des Oszilloskops}
Der grundsätzliche Aufbau eines Oszilloskops ist in \cref{fig:osziaufbau} zu sehen. Der Schirm visualisiert die gemessenen Größen. Dabei können die Spannungen der einzelnen Kanäle gegen die Zeit (dafür dient die Zeitbasis) oder auch gegeneinander aufgetragen werden (das regelt die \glqq Mode\grqq \ - Einheit). \\
Mit Hilfe des Triggers kann auf dem Bildschirm ein stehendes Bild erzeugt werden, indem die Darstellung jeweils für einen Bildschirmdurchlauf gestartet wird, wenn das Messsignal einen bestimmten Triggerpegel über- oder unterschreitet. Das Konzept ist in \cref{fig:trig} visualisiert. 
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{osziaufbau.png}
	\caption{Grundsätzlicher Aufbau eines Oszilloskops}
	\label{fig:osziaufbau}
\end{figure}\\
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{trigger.png}
	\caption{Prinzip des Triggervorgangs}
	\label{fig:trig}
\end{figure}

\subsection{Tastkopf}
Der Tastkopf, dessen Funktionsweise in \cref{fig:tk} schematisch dargestellt ist, kann bei Messungen die Kapazität von Oszilloskop und Zuleitungen ignorieren.
\begin{figure}[h]
\centering
	\includegraphics[scale=0.4]{tastkopf.png}

	\caption{Prinzip des Tastkopfes}
	\label{fig:tk}
\end{figure}
Das Prinzip ist dabei das einer Wheatstone'schen Brückenschaltung. Genau dann, wenn das Teilverhältnis der Kapazitäten gleich dem der Widerstände ist, fließt kein Ausgleichsstrom über die Mittelverbindung und die Spannungsteiler beeinflussen sich gegenseitig nicht. Das erkennt man daran, dass das Eingangssignal $U_E$ gleich dem Ausgangssignal (auf dem Oszilloskop) ist. Die Wirkung der Kapazität von Oszilloskop und Zuleitungen ist dadurch unterdrückt.
Es gilt dann gemäß Spannungsteilerregel
\begin{align}
\frac{C_\text{Oszi}+C_\text{Kabel}}{C_\mathrm{Tastkopf}}=\frac{\SI{1}{M\Omega}}{\SI{9}{M\Omega}}=\frac 1 9 .
\label{eq:tk}
\end{align}
\subsection{Wechselstromschaltungen}
Einfache Wechselstromschaltungen bestehen Widerständen $R$, Kapazitäten $C$ und Induktivitäten $L$. Sie lassen sich, wenn die Eingangsspannung einem Sinussignal mit Frequenz $\omega$ entspricht, mittels komplexer Wechselstromrechnung beschreiben. Dabei sind die komplexen Widerstände der oben genannten Bauteile gleich
\begin{align*}
Z_R &= R\\
Z_C &= \frac{1}{i \omega C}=\frac{-i}{\omega C}\\
Z_L &= i\omega L.
\end{align*}
Das bedeutet, dass ideale Spulen eine Phasendifferenz von $\frac \pi 2$ zwischen Strom und Spannung aufweisen (der Strom ist verspätet) und ideale Kondensatoren $-\frac \pi 2$. Zwei grundlegende Schaltungen sind Hoch- und Tiefpass, wie in \cref{fig:ht} dargestellt.\\
Mit Hilfe der komplexen Spannungsteilerregel kann die an Hoch- und Tiefpass abgegriffene Spannung in Abhängigkeit von $\omega$, $R$ und $C$ ausdrücken gemäß
\begin{align}
U_\text{A,TP}&=U_E \frac 1 {1+ i\omega RC}\\
U_\text{A,HP}&=U_E \frac {i\omega RC} {1+ i\omega RC}
\label{eq:hochtief}
\end{align}
\begin{wrapfigure}{r}{9cm}
	\vspace{20pt}
	\includegraphics[scale=0.33]{hochtief.png}
	\caption{(a) ist die Tiefpassschaltung, (b) der Hochpass}
	\vspace{-46pt}
	\label{fig:ht}
\end{wrapfigure}
Als Durchgangskurve bezeichnet man die Größen
\begin{align}
g_\text{TP}&=\left| \frac{U_\text{A,TP}}{U_E} \right|= \frac 1 {\sqrt{1+ (\omega RC)^2}}\\
g_\text{HP}&=\left| \frac{U_\text{A,HP}}{U_E} \right|= \frac {\omega RC} {\sqrt{1+ (\omega RC)^2}}
\label{eq:durch}
\end{align}
Zudem definiert man noch die Grenzfrequenz $\omega_G :=\frac 1 {RC}$. Die Phasenverschiebung zwischen $U_A$ und $U_E$ ergibt sich zu
\begin{align}
\phi_\text{TP}&=\arctan(-\omega RC)\\
\phi_\text{HP}&=\arctan \left(\frac 1 {\omega RC} \right)
\label{eq:Phase}
\end{align}
Für hochfrequente Signale bei einem Tiefpass fällt fast die gesamte Eingangsspannung am Widerstand ab, also gilt
\begin{align*}
U_E \approx R I = R \frac{dU_C}{dt} C
\end{align*}
Die zweite Gleichheit folgt damit aus der Tatsache, dass im gesamten Stromkreis der gleiche Strom $I$ fließt und dass man den Strom durch einen Kondensator gemäß $I=C \dot U $ ausdrücken kann. $U_C$ ist die abgegriffene Spannung, also gilt
\begin{align}
U_\text{A,TP}=\omega_G \int U_E dt.
\label{eq:intwirk}
\end{align}
Ein Tiefpass hat somit eine integrierende Wirkung.\\
Analog kann man dem Hochpass eine differenzierende Wirkung zuschreiben: Für kleine Frequenzen fällt die Spannung fast komplett am Kondensator ab und daher ist
\begin{align}
U_\text{A,HP}=RI=RC\frac{dU_C}{dt} \approx RC\frac{dU_E}{dt}.
\label{eq:diffwirk}
\end{align}

\subsection{Schwingkreise}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.45]{schwingkreis.png}
	\caption{Grundschaltungen für Schwingkreise: (a) ohne äußere Spannungsquelle, (b) in Serienschaltung und (c) in Parallelschaltung}
	\label{fig:schwing}
\end{figure}
Ein Schwingkreis, siehe auch \cref{fig:schwing} besteht aus einer Kapazität, Induktivität und einem Widerstand. Diese können in Reihe oder parallel geschaltet sein. Ihre Beschreibung führt auf die Differentialgleichgung
\begin{align}
U_\text{E} =L\ddot Q + R \dot Q + \frac 1 C Q \label{eq:dgl}
\end{align}
für die Ladung $Q$ auf dem Kondensator. Diese hat für $U_\text{E} = U_\text{E,0}\cos{\omega t}$ für den Strom $\dot Q$ die Lösung $I(t)=I_0 \cos(\omega t -\phi)$ mit
\begin{align*}
    I_0=\frac{U_\text{E,0}}{\sqrt{R^2+\left( \omega L-\frac {1} {\omega C} \right) ^2}} \\
    \tan(\phi)=\frac{\omega L-\frac 1 {\omega C} }{R}.
\end{align*}
Man erhält mit $U_A(t)=R_m I(t)$ für die Durchgangskurve
\begin{align}
    g_\text{SSchw}&=\frac{R_\text{m}}{\sqrt{R^2+\left( \omega L-\frac 1 {\omega C} \right) ^2}} 
                   = \frac{R_\text{m}}{L} \frac{\omega}{\sqrt{4 \delta^2 \omega^2 + (\omega^2 + {\omega_0}^2)^2}}   \label{eq:gRLC}\\
    \tan(\phi)&=\frac{\omega L-\frac 1 {\omega C} }{R} = \frac{(\omega - {\omega_0}^2)}{2 \delta \omega}  \label{eq:phiRLC} ~,
\end{align}
wobei $\omega_0 = 1/\sqrt{L C}$ and $\delta = R / 2 L$.

Analoge Überlegungen gelten für den Parallelschwingkreis.\\
Als Resonanzfrequenz bezeichnet man jene Frequenz, für die der Scheinwiderstand reell, Strom und Spannung folglich in Phase sind. Das führt im Serienschwingkreis mittels $\tan(\phi)=\frac{\omega L-\frac 1 {\omega C} }{R}$ auf $\omega_\text{res}=\sqrt{\frac 1 {LC}}$.\\
Im Parallelschwingkreis gilt 
\begin{align}
\omega_\text{res}=\sqrt{\frac 1 {LC} - 4 \delta^2}
\label{eq:Parralelresonanz}
\end{align}
mit Dämpfungskonstante $\delta=\frac R {2L}$.\\
Als Bandbreite $B_\omega$ ist der Abstand zwischen den beiden Kreisfrequenzen definiert, bei der die Durchlasskurve auf das $1/\sqrt 2$-fache des Maximalwertes abgesunken ist. Es gilt 
\begin{align}
B_\omega =2\delta = \frac R L.
\label{eq:BB}
\end{align}
Die Güte $Q$ eines Schwingkreises ist definiert als
\begin{align}
Q=\frac{\omega_\text{res}}{B_\omega}=\frac 1 R \sqrt{\frac L C}.
\label{eq:Guete}
\end{align}
\section{Versuchsdurchführung}
\subsection{Kalibration des Tastkopfes}
Der Tastkopf wurde mit einem Rechtecksignal kalibriert, indem seine Kapazität $C_\text{Tastkopf}$ so eingestellt wurde, dass das Rechteckssignal auch als Ausgabesignal beobachtet werden konnte.
\subsection{Hoch- und Tiefpass}
Durch Messen der Eingangs- und Ausgangsspannung an einem Tiefpass konnten wir dessen Durchlasskurve und Phasenverschiebung messen. Anschließend wurde die integrierende bzw. differenzierende Wirkung mit Dreiecks- und Rechtecksfunktionen als Eingangssignal überprüft.
\subsection{Serienschwingkreis}
Analog zum Tiefpass wurde die Durchlasskurve und Phasenverschiebung eines Serienschwingkreises vermessen und dessen Resonanzfrequenz bestimmt.\\
Durch Messen des Abklingens eines Spannungssprungs (in Form eines Rechteckssignals) wollen wir die Dämpfung des Schwingkreises bestimmen.
\subsection{Koaxialkabel}
Zum Schluss wird mit Hilfe des Tastkopfes die Kapazität eines Koaxialkabels bestimmt, indem Resonanzfrequenz des Schwingkreises einmal ohne und einmal mit Koaxialkabel in Serie zum Schwingkreis bestimmt wird.