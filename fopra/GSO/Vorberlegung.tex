\section{Vorüberlegung}
Zur Messung von Lichtintensitäten kann eine Photodiode verwendet werden. Dort lösen sich durch einfallendes Licht Elektronen, die dann zu einem Photostrom $I_\text{Ph}$ führen. Der Quantenwirkungsgrad, welcher angibt, wie wahrscheinlich es ist, dass ein Photon bei gegebener Wellenlänge ein Elektron loslöst, kann dabei laut Angabe als etwa $\eta_Q =75\%$ angenommen werden.
Der vom Licht hervorgerufene Stromfluss kann als über einem Messwiderstand $R_M$, der je nach Diode variiert, anliegende Spannung $U$ gemessen werden. Nennt man den einfallenden Photonenfluss $\dot n$, kann man die Leistung eines Lasers mit Wellenlänge $\lambda$ berechnen als
\begin{align}
P=\dot W =\dot n h \nu = \dot n \frac{hc}\lambda = \frac {I_\text{Ph}}{e \eta _Q}\ \frac{hc}\lambda=\frac {U}{R_M e \eta _Q}\ \frac{hc}\lambda \label{eq:Leistung}
\end{align}
\subsection{Gauß'sche Strahlen}
Gauß'sche Strahlen können in durch zwei Parameter charakterisierte Moden auftreten. Die sogenannte $\text{TEM}_{0,0}$-Moden können durch zwei Parameter beschrieben werden, beispielsweise ihren \textit{waist} $w_0$, ein Maß für den Strahldurchmesser an der dünnsten Stelle und dessen Position. Daraus lassen sich weitere Größen berechnen wie zum Beispiel Rayleighlänge $z_\text{R}$ (ein Maß dafür, wie stark der Strahl divergiert), der Strahlradius $w(z)$ bei einem Abstand $z$ vom waist oder der Krümmungsradius $R(z)$ der Wellenfronten. Es gilt
%
\begin{align}
	z_\text{R} = \frac{\pi w_\text{0} ^2 n}\lambda \label{eq:zR}\\
	w(z)=w_\text{0} \sqrt{1+\frac {z^2}{z_\text{R} ^2}} \label{eq:durchmesser}\\
	R(z)=z\left(1+\frac {z_\text{R} ^2}{z^2}\right) \label{eq:krmrad}.
\end{align}
%
Hierbei bezeichnet $n \approx 1$ den Brechungsindex von Luft und $\lambda$ die Wellenlänge des Lasers. Für $z \gg z_\mathrm{R}$ ergibt sich aus \cref{eq:durchmesser}
%
\begin{equation}
	w(z) \approx w_0 \frac{z}{z_R} = \frac{\lambda}{\pi w_0} z \label{eq:approx_w0}
\end{equation}
%
Gauß'sche Strahlen verändern sich durch optische Bauteile gemäß dem ABCD-Gesetz: Wenn
\begin{equation*}
\begin{pmatrix}
	A&B\\
	C&D
\end{pmatrix}
\end{equation*}
die zum Bauteil gehörende Transfermatrix ist und $q=z+iz_\text{R}$ die Position des Bauteils im Strahl charakterisiert, so lässt sich der Strahl nach dem Bauteil beschreiben durch
\begin{align*}
q' = \frac {Aq+B}{Cq+D}.
\end{align*}
\subsubsection*{Präparation eines Laserstrahls mit bestimmtem Waist}
Zwei Linsen im Abstand $d$ mit Brennweiten $f_1$ und $f_2$ haben eine Transfermatrix von
\begin{equation*}
\begin{pmatrix}
1&0\\ -\frac 1 {f_1}&1
\end{pmatrix}
\begin{pmatrix}
1&d\\ 0&1
\end{pmatrix}
\begin{pmatrix}
1&0\\ -\frac 1 {f_2}&1
\end{pmatrix}=
\begin{pmatrix}
1-\frac d {f_2}&d\\ \frac {d-f_1-f_2} {f_1 f_2}&1-\frac d{f_1}.
\end{pmatrix}
\end{equation*}
Also gilt mit $q=iz_\text{R}$ (da die erste Linse im Fokus steht)
\begin{align*}
z_\text{R} ' = \text{Im}(q')= \text{Im}\left(\frac{iz_\text{R} \left(1-\frac d {f_2}\right) + d}{iz_\text{R} \frac{d-f_1-f_2}{f_1 f_2} +1-\frac d {f_1}}\right)
\end{align*}
Drückt man nun $z_\text{R}$ und $z_\text{R} '$ gemäß \cref{eq:zR} mit $\lambda = \SI{632.8}{nm}$ durch $w_0$ aus, verwendet $f_1=\SI{50}{mm}$ und $f_2=\SI{100}{mm}$ (also die stärker brechende Linse platziert man zuvorderst), so kann man diese Gleichung lösen (lassen von Mathematica) und erhält $d\approx \SI{351.4}{mm}$.
\subsection{Resonatoren}
Fabry-P\'{e}rot-Resonatoren bestehen aus zwei teilreflektierenden Spiegeln im Abstand $L$ und können dazu verwendet werden, Licht einer bestimmten Wellenlänge zu erzeugen. Die typische Kenngröße eines solchen Resonators ist die Finesse $F$, die als Quotient aus Halbwertsbreite eines und Abstand zweier Transmissionspeaks definiert werden kann.\\
Man kann die Finesse mit geeigneten Annahmen auch aus dem geometrischen Mittel $R=\sqrt{R_1R_2}$ der Reflexionsgrade der beiden Spiegel berechnen. Es gilt
\begin{align}
F=\frac{\pi \sqrt R}{1-R} . \label{eq:Reso}
\end{align}
Außerdem kann der Waist der Eigenmode im Resonator durch den Krümmungsradius $R_c$, den Abstand $L$ der Spiegel und die Wellenlänge $\lambda$ ausgedrückt werden, es ist
\begin{align}
w_0^2 = \frac \lambda \pi \sqrt{\frac L 2 \left(R_c - \frac L 2\right)} \label{eq:reswaist}
\end{align} 