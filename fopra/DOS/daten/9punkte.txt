#Grid-Scan using Optidos
#Timet: 27-01-20-11:52:14
#Unit: Gy
#Duration: 20 s
#
#Aufloesung:
#  0 besser als 0.5%
#  1 zwischen 0.5% und 1%
#  2 schlechter als 1%
#Meßbereich:
#  0 Low
#  1 High
#
x[mm];y[mm];z[mm];Value;Resolution;Range
23.9;25.9;0;0.1368;0;0
24;25.9;0;0.1425;0;0
24.1;25.9;0;0.1479;0;0
24.1;26;0;0.1545;0;0
24;26;0;0.1487;0;0
23.9;26;0;0.1429;0;0
23.9;26.1;0;0.148;0;0
24;26.1;0;0.1539;0;0
24.1;26.1;0;0.1597;0;0
