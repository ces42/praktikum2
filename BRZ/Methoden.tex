\section{Einleitung}

Bei Brennstoffzellen geht es darum, durch chemische Reaktion von Wasserstoff und Sauerstoff 
elektrische Energie zu erzeugen. Hierzu werden die benötigten Gase zuerst in einem Elektrolyseur aus Wasser erzeugt.
Anschliessend können diese in die Brennstoffzelle geleitet werden, in der sie wieder Wasser bilden und dabei chemische Energie freigeben.

\section{Theoretische Grundlagen}

Im folgenden werden grundlegend die Funktion der Brennstoffzelle und des Elektrolyseurs erklärt, sowie die Begriffe der Kennlinie und des Wirkungsgrades erläutert.

\subsection{PEM-Brennstoffzelle}

\begin{figure}[h]
        \centering
        \includegraphics[width=0.5\textwidth]{pem-brennstoffzelle.jpg}
        \caption{Schematischer Aufbau einer PEM-Brennstoffzelle}
        \label{fig:Brennstoffzelle}
\end{figure}

Der Aufbau einer Brennstoffzelle ist schematisch in Abb. \ref{fig:Brennstoffzelle} dargestellt.

Die Brennstoffzelle besteht aus einer Anode, an der der Brennstoff zuströmt und einer Kathode, an der das Oxidationsmittel zuströmt.
Der Brennstoff ist dabei Wasserstoff (\ce{H2}) und das Oxidationsmittel Sauerstoff (\ce{O2}). Die Gase werden jeweils durch Gasflussplatten über die gesamte Elektrodenfläche verteilt, gleichzeitig wird über diese das entstehende Wasser abgeführt. Die beiden Elektroden sind zusätzlich mit sehr fein verteiltem Platin beschichtet, dieser dient als Katalysator. Zwischen den beiden Elektroden befindet sich eine Polymerelektrolytmembran, die nur für Protonen durchlässig ist und daher die Gase trennt (PEM = photon exchange membran). \newline

An der Anode oxidiert \ce{H2},

\begin{equation*}
    \ce{H2 -> 2H+ + 2e-} 
\end{equation*}

und setzt dabei Elektronen frei.
An der Kathode erfolgt die Reduktion des \ce{O2}, hierbei werden Elektronen benötigt:

\begin{equation*}
    \ce{O2 + 4e- -> 2O^{2-}}.
\end{equation*}

Die Reaktionen erfolgen beide freiwillig. Bei der exothermen Gesamtreaktion

\begin{equation*}
    \ce{O2 + 2H2 -> 2O^{2-} + 4H+}
\end{equation*}

wird Energie frei.
Ist der Stromkreis geschlossen, fließt ein Strom. \newline
Die bei der Reaktion entstehenden \ce{H+}-Ionen können durch die Membran diffundieren und verbinden sich an der Kathode mit den dort entsthehenden Sauerstoffionen \ce{O^{2-}}, es entsteht Wasser (\ce{H2O}) als Reaktionsprodukt. Damit die Brennstoffzelle nicht überflutet wird, muss für eine kontinuierliche Abführung des Wassers gesorgt werden. \newline

Das als Katalysator verwendete Platin senkt bei der Reaktion die benötigte Aktivierungsenergie und führt damit zu einer Erhöhung der Reaktionsgeschwindigkeit.

\subsection{Elektrolyseur}

Mit dem Elektrolyseur lassen sich die zwei Gase \ce{H2} und \ce{O2} aus Wasser (\ce{H2O}) erzeugen. Die Funktionsweise ist ähnlich der der Brennstoffzelle, allerdings laufen die Vorgänge hier umgekehrt ab. Bei angelegter Spannung wird an der Anode Wasser aufgespalten

\begin{equation*}
    \ce{2H2O -> O2 + 4H+ + 4e-},
\end{equation*}

und an der Kathode entsteht Wasserstoff

\begin{equation*}
    \ce{2H+ + 2e- -> H2}.
\end{equation*}

Die dabei benötigten \ce{H+}-Ionen diffundieren durch die Membran von der Anode zur Kathode.
Die Gesamtreaktion ist endotherm, es muss Energie zugeführt werden.

\subsection{Kennlinie der Brennstoffzelle}

\begin{figure}[h]
        \centering
        \includegraphics[width=0.8\textwidth]{Kennlinie.PNG}
        \caption{Kennlinie einer Brennstoffzelle. Die drei Bereiche können klar unterschieden werden.}
        \label{fig:Kennlinie}
\end{figure}

Eine ideale Brennstoffzelle verhält sich wie eine Konstantspannungsquelle. Die theoretisch mögliche, ideale Spannung beträgt $E_0 = 1.23\,\text{V}$
(entspricht der Mindestspannung für Elektrolyse von Wasser).
Nun ist die Abweichung der Brennstoffzelle vom idealen Verhalten von Interesse, dazu betrachten wir die Kennlinie, die Abhängigkeit der Zellspannung $U$ vom Zellstrom $I$. Die Abweichung der Zellspannung von der theoretisch möglichen Spannung wird Überspannung genannt und hat mehrere Ursachen. Diese Ursachen sind in verschiedenen Bereichen der Kennlinie besonders gut zu erkennen.

\subsubsection{Durchtritts-Überspannung}

Die Durchtrittsüberspannung $\eta_\text{Durch}$ entsteht durch die endliche Geschwindigkeit des Ladungsdurchtritts der Elektronen durch die Phasengrenze Elektrode/Elektrolyt. Sie kommt vor allem im Bereich kleiner Zellströme, dem sogenannten elektrokinetischen Bereich vor. \newline

Die Durchtrittsüberspannung wird durch die Butler-Volmer-Gleichung beschrieben, welche aus zwei Termen (jeweils für Oxidation und Reduktion) besteht. Ist die Überspannung ausreichend groß, so kann man in der Gleichung jeweils eine Reaktion vernachlässigen und gelangt zu zwei einfacheren Gleichungen für Oxidation und Reduktion. Von den beiden Reaktion ist die Oxidation von \ce{O2} die langsamere, sie bestimmt also die Geschwindigkeit und damit das Verhalten der Kennlinie im elektrokinetischen Bereich.

Aus der Reduktionsgleichung erhält man durch Umformungen die Tafelgleichung für die Durchtrittsüberspannung


\begin{equation}
    \eta_\text{Durch} = \frac{R T}{(1-\alpha)z F} \ln\left(\frac{I}{-I_0}\right),
    \label{eq:Tafelgleichung}
\end{equation}

Mit den Konstanten $F$ (Faraday-Konstante) und $R$ (allgemeine Gaskonstante) und der Anzahl der durchtretenden Elektronen $z$, die in unserem Fall $z=2$ beträgt, da wir uns auf Wasserstoffmoleküle \ce{H2} beziehen.
Der Durchtrittsfaktor $\alpha$ liegt zwischen  0 und 1 und gibt den Anteil von $\eta_\text{Durch}$ an, der für die
Aktivierungsenergie der anodische Oxidation benötigt wird. Der Anteil der Aktivierungsenergie der kathodischen Reduktion entspricht also  $1 - \alpha$. \newline
Der Austauschstrom $I_0$ beschreibt die Größe des Stoffaustausches durch anodische und kathodische Reaktion
am Gleichgewichtspotential (dynamisches Gleichgewicht). Dieser Strom liegt zum  Beispiel an einer ablaufenden Reaktion oder an einer anliegenden Spannung. \newline

Im Experiment kann der Stom $I$ und die Durchtrittsspannung $\eta_\text{Durch}$ gemessen und damit $\alpha$ und $I_0$ bestimmt werden.

\subsubsection{Ohmsche Überspannung}

Die Ohmsche Überspannung $\eta_\text{Ohm}$ entsteht durch den inneren Widerstand der Brennstoffzelle. Da die Spannung linear mit dem Strom abnimmt steigt die Ohmsche Überspannung mit dem Strom gemäß

\begin{equation}
    \eta_\text{Ohm} = R_\text{in} \cdot I.
    \label{eq:OhmscheUeberSpannung}
\end{equation}

Die Ohmsche Überspannung spielt im gesamten Bereich der Kennlinie eine Rolle.

\subsubsection{ Diffussionsüberspannung}

Die Diffusionsüberspannung $\eta_\text{Diff}$ entsteht durch die Diffusion der Gase durch die porösen Elektroden zum Katalysator und zum Elektrolyten.
Sie tritt erst bei hohen Strömen auf, wenn die Gase durch die Reaktion am Katalysator schneller verbraucht werden, als sie durch Diffusion dorthingelangen können. Dieses Phänomen äußert sich in einem plötzlichen Abknicken der Diffusionslinie nach unten, die Spannung nimmt mit steigendem Strom stark ab. Ist der Grenzstrom $I_\text{Grenz}$ erreicht, sinkt die Spannung ganz auf Null ab.

\subsubsection{Zusammenfassung}

Zusammengefasst beträgt die Spannung der Brennstoffzelle also

\begin{equation}
    U = E_0 - \eta_\text{Durch} - \eta_\text{Ohm} - \eta_\text{Diff}
      = E_0 - \frac{R T}{(1-\alpha)z F} \ln\left(\frac{I}{-I_0}\right) - R_\text{in} \cdot I - \eta_\text{Diff}.
      \label{eq:GesamteUeberspannung}
\end{equation}

der Einfluss von $\eta_\text{Diff}$ spielt bei kleinen Strömen noch keine Rolle, da an der Elektrode
mehr Gas angeboten wird, als verbraucht werden kann. $\eta_\text{Ohm}$ ändert sich im gesamten Bereich linear mit dem Strom.
$\eta_\text{Durch}$ ändert sich bei kleinen Strömen stark, und ist für große Ströme nahezu konstant.
Die Kennlinie der Brennstoffzelle ist in Abb. \ref{fig:Kennlinie} zu sehen

\subsection{Wirkungsgrad}

Der Wirkungsgrad ist ein Maß für das Verhältnis von Nutzen zu Aufwand. In diesem Versuch werden zwei
verschiedene Wirkungsgrade betrachtet.

\subsubsection{Faraday-Wirkungsgrad}

Der Faraday-Wirkungsgrad $\epsilon_F$ ist das Verhältnis vom im Experiment pro Zeit umgesetzten zum theoretisch umsetzbaren Wasserstoffvolumen.
Das theoretische umsetzbare Wasserstoffvolumen kann über die Anzahl der Ladungen errechnet werden, bei einem elektrischen Strom $I$
ergibt sich ein Volumen pro Zeit von

\begin{equation}
\dot{V}_{H_2, \text{Theo}} = \frac{d V_{H_2, \text{Theo}}}{dt} = \frac{I \cdot V_m}{z \cdot F},
\label{eq:theoWasserstoffvolumen}
\end{equation}

mit dem molaren Gasvolumen bei Laborbedingungen $V_m$.

Für den Elektrolyseur ist dabei das experimentell gemessene Volumenänderung der Nutzen, der Stromfluss ist
der Aufwand, es gilt also

\begin{equation}
    \epsilon_\text{F, Elektrolyseur} = \frac{\dot{V}_{H_2, \text{Exp}}}{\dot{V}_{H_2, \text{Theo}}}
    \label{eq:FaradyElektrolyseur}
\end{equation}

und für die Brennstoffzelle gilt folglich

\begin{equation}
    \epsilon_\text{F, Brennstoffzelle} = \frac{\dot{V}_{H_2, \text{Theo}}}{\dot{V}_{H_2, \text{Exp}}}.
    \label{eq:FaradyBrennstoffzelle}
\end{equation}

\subsubsection{Energiewirkungsgrad}

Der Energiewirkungsgrad $\epsilon_E$ vergleicht den Energieinhalt des pro Zeit verbrauchten Gases mit der gewonnenen bzw. verbrauchten elektrischen Energie $W = U \cdot I \cdot t=P\cdot t$.
Der chemische Energiegehalt von \ce{H2} beträgt dabei $\Delta H^0 =  286 \, \frac{kJ}{mol}$.

Folglich gilt für den Energiewirkungsgrad des Elektrolyseurs

\begin{equation}
    \epsilon_\text{E, Elektrolyseur} = \frac{\Delta H^0 \cdot n}{W_{in}}=\frac{\Delta H^0 \cdot \frac{n}{t}}{P_{in}}
    \label{eq:EnergieElektrolyseur}
\end{equation}

und für den der Brennstoffzelle

\begin{equation}
    \epsilon_\text{E, Brennstoffzelle} = \frac{P_{out}}{\Delta H^0 \cdot \frac{n}{t}}
    \label{eq:EnergieBrennstoffzelle}
\end{equation}

mit der umgesetzten Stoffmenge pro Zeit $\frac{n}{t}$ in $\SI{}{mol \per s}$ und der verbrauchten bzw. gewonnen Leistung $P_{in}$ bzw. $P_{out}$.

\section{Versuchsdurchführung}

Im Versuch wurde mit einem Elektrolyseur Wasserstoff und Sauerstoff erzeugt, anschließend konnten zwei Brennstoffzellen benutzt werden um die in den zwei Gasen enthaltene chemische Energie in elektrische Energie umzuwandeln.

\subsection{Charakterisierung des Elektrolyseurs}

Zuerst wurden einige Versuche am Elektrolyseur durchgeführt um dessen Leckrate, Kennlinie und Gaserzeugungsrate zu bestimmen.

\subsubsection*{Bestimmung der Leckrate}

Um die Leckrate zu bestimmen wurde mit dem Elektrolyseur ein Gasvorrat von etwa $30\,\text{ml}$ erzeugt. Dann wurden alle elektrischen Verbindungen getrennt und der Gaspegel über einen Zeitraum von 10 Minuten beobachtet. Das Gasvolumen wurde einmal pro Minute notiert.

\subsubsection*{Bestimmung der Kennlinie}

Als nächstes wurden die Daten gemessen, die für die Erstellung eines Strom-Spannungs-Diagramms notwendig sind.
Hierfür wurde der Strom in $0,1\,\text{A}$ Schritten variiert und jeweils die Spannung gemessen. Die Spannung, bei der der Strom einsetzt ($U(I\xrightarrow{}0)$) wurde besonders genau mit mehreren Messungen bestimmt.

\subsubsection*{Bestimmung der Gaserzeugungsrate}

Zuletzt wurde mit einem Strom von ungefähr $1\,\text{A}$ Gas erzeugt. Der Füllstand des Wasserstoffreservoirs wurde über einen Zeitraum von 6 Minuten alle 30 Sekunden gemessen.

\subsection{Charakterisierung der Brennstoffzellen}

Die Brennstoffzellen wurden mit Wasserstoff und Sauerstoff aus dem Elektrolyseur versorgt. zwischen den einzelnen Versuchen mussten die Brennstoffzellen immer wieder mit dem Gas gespült, um eine Flutung der Brennstoffzelle mit dem erzeugten Wasser zu verhindern. Hierzu wurden die klemmen geöffnet, die das Entweichen des Gases durch die Brennstoffzellen verhindern.\newline

Zur Charakterisierung der Brennstoffzellen wurden die Verbrauchsrate und die Kennlinien der Brennstoffzellen (einmal in Parralel- und einmal in Reihenschaltung) bestimmt.

\subsubsection*{Bestimmung der Verbrauchsrate}

Zunächst wurden die beiden Brennstoffzellen zu einer Parallelschaltung verbunden. Bei einem Lastwiderstand von $0.3\,\Omega$ wurde nun die Zeit bestimmt, in der ein Gasvolumen von $30\,\text{ml}$ verbraucht wurde. In Zeitintervallen von 30 Sekunden wurde dabei der Füllstand sowie die Spannung und der Strom der Brennstoffzellen bestimmt.

\subsubsection*{Bestimmung der Kennlinie}

Die Brennstoffzellen wurden nun in Parallelschaltung betrieben. Für verschiedene Widerstände wurden jeweils Strom und Spannung gemessen, es wurden drei Messreihen durchgeführt. 

Danach wurden die Brennstoffzellen zu einer Reihenschaltung verbunden und die Messungen erneut durchgeführt.
